FetchUserData = function(regNo)
{
	var params =
	{
		cVerb: "GET",
		cURL: "http://localhost/PdfForms/api_user.php?regno=" + regNo,
		oHandler:
		{
			response: function(msg, uri, e)
			{
				if(e != undefined && msg.error != undefined) {
					app.alert("Failed to : "+ msg.error);
				} else {
					app.alert("Fetched: " + msg);
				}
			}
		}
	};
	Net.HTTP.request(params);
}
var prefilBtn = this.getField("prefill_data");
var regno = this.getField("regno");
prefilBtn.setAction("MouseUp", "FetchUserData("+regno.value+")");

var name = this.getField("name");
var email = this.getField("email");
var phone = this.getField("phone");
var address = this.getField("address");
