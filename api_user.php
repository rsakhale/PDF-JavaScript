<?php

require_once 'db.php';

$query = "SELECT * FROM `users` ";

if (isset($_GET['regno'])) {
    $query .= " WHERE `regno` = '" . $mysqli->escape_string($_GET['regno']) . "'";
}

$result = $mysqli->query($query);

if ($result && $result->num_rows > 0) {
    header("HTTP/1.0 200 Ok");
    $arr = [];
    while ($row = $result->fetch_assoc()) {
        $arr[] = $row;
    }
    if (count($arr) == 1) {
        echo json_encode($arr[0]);
    } else {
        echo json_encode($arr);
    }
}
header("HTTP/1.0 404 Not Found");
header("Content-Type: application/json");
echo json_encode(['error' => 'Something went wrong']);
