<!doctype html>
<html>
	<head>
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PDF JavaScript based form generator</title>

    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="index.php">PDF with JS</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	          	<li class="navbar-text">Developed by</li>
	            <li><a href="https://rohansakhale.com">Rohan Sakhale</a></li>
	            <li class="navbar-text">For</li>
	            <li><a href="http://consultlane.com">Consultlane.com</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	     <div class="container">

	      <!-- Main component for a primary marketing message or call to action -->
	      <div class="jumbotron">
	        <h1>Generate PDF with JavaScript</h1>
	        <p>This example is presented for <strong>ConsultLane.com</strong> for generating PDF inclusive of JavaScript.</p>
	        <p>
	          <a class="btn btn-lg btn-primary" href="generate_simple_form_pdf.php" role="button">Generate Simple PDF &raquo;</a>
	          <a class="btn btn-lg btn-primary" href="generate_prefill_data_pdf.php" role="button">Generate Pre-fill Data PDF &raquo;</a>
	          <a class="btn btn-lg btn-primary" href="generate_with_data_pdf.php" role="button">Generate With Data PDF &raquo;</a>
	        </p>
	      </div>
	      <div class="row">
		      <div class="col-sm-4">
			      <h2>Sample Form</h2>
			      <?php require 'form.php';?>
		      </div>
		      <div class="col-sm-8">
		      	<form action="save_pdf.php" method="POST">
		      	<div class="form-group">
				<div class="row">
					<label class="col-sm-2 control-label">RegNo</label>
					<div class="col-sm-9">
					<input class="form-control" type="text" name="regno" size="30" maxlength="64" />
					</div>
				</div>
				</div>
				<div class="form-group">
				<div class="row">
					<label class="col-sm-2 control-label">Name</label>
					<div class="col-sm-9">
					<input class="form-control" type="text" name="name" size="30" maxlength="64" />
					</div>
				</div>
				</div>
				<div class="form-group">
				<div class="row">
					<label class="col-sm-2 control-label">Email</label>
					<div class="col-sm-9">
					<input class="form-control" type="text" name="email" size="30" maxlength="64" />
					</div>
				</div>
				</div>
				<div class="form-group">
				<div class="row">
					<label class="col-sm-2 control-label">Phone</label>
					<div class="col-sm-9">
					<input class="form-control" type="text" name="phone" size="30" maxlength="64" />
					</div>
				</div>
				</div>
				<div class="form-group">
				<div class="row">
					<label class="col-sm-2 control-label">Address</label>
					<div class="col-sm-9">
					<input class="form-control" type="text" name="address" size="30" maxlength="64" />
					</div>
				</div>
				</div>
				<input class="btn btn-primary btn-lg" type="submit" name="submit" />
				</form>

		      </div>
	      </div>
	    </div> <!-- /container -->

	</body>
</html>
