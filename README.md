# PDF Forms

This is simple project that helps generate quick PDF files using **TCPDF** library

## Installation

Install TCPDF dependency using following command

```
composer install
```

## Run

Run this project over your webserver and a quick bootstrap index page will appear showing two buttons one for simple forms and another for pre-filled data kind of forms.

These are just references and needs to dig in more to get better results.

## Author

Rohan Sakhale <rohansakhale@gmail.com>

## License

GNU GPLv3