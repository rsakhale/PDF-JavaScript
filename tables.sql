CREATE TABLE `pdfform`.`users` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `regno` VARCHAR(16) NOT NULL ,
    `name` VARCHAR(64) NOT NULL ,
    `email` VARCHAR(64) NOT NULL ,
    `phone` VARCHAR(16) NOT NULL ,
    `address` VARCHAR(256) NOT NULL ,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL ,
    `deleted_at` DATETIME NULL DEFAULT NULL ,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB charset='UTF8';

INSERT INTO `users` (`id`, `regno`, `name`, `email`, `phone`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES 
(NULL, 'A123', 'Rohan Sakhale', 'rohansakhale@gmail.com', '8976379661', 'A/104, Narayan Smruti, Bhayandar West', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'B234', 'Rajan Sakhale', 'rajansakhale@gmail.com', '9892123797', 'A/104, Narayan Smruti, Bhayandar West', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'C456', 'Hello World 1', 'helloworld1@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'D456', 'Hello World 2', 'helloworld2@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'E456', 'Hello World 3', 'helloworld3@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'F456', 'Hello World 4', 'helloworld4@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'G456', 'Hello World 5', 'helloworld5@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'H456', 'Hello World 6', 'helloworld6@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'I456', 'Hello World 7', 'helloworld7@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'J456', 'Hello World 8', 'helloworld8@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'K456', 'Hello World 9', 'helloworld9@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'H456', 'Hello World 10', 'helloworld10@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL),
(NULL, 'I56', 'Hello World 11', 'helloworld11@gmail.com', '9892098920', 'Hello World Gaav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL);