<?php

require_once 'vendor/autoload.php';

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Rohan Sakhale');
$pdf->SetTitle('Forms with JS');
$pdf->SetSubject('Fetch data using JavaScript');
$pdf->SetKeywords('javascript,forms,php,pdf');
// set default header data
$header_logo = "http://localhost/PdfForms/images/SaiAshirwadInformatia_232.png";
$header_title = "Forms with JavaScript";
$header_string = "Presented by Rohan Sakhale";
//$pdf->SetHeaderData($header_logo, 280, $header_title, $header_string);
// set header and footer fonts
$pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
$pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// IMPORTANT: disable font subsetting to allow users editing the document
$pdf->setFontSubsetting(false);
// set font
$pdf->SetFont('helvetica', '', 10, '', false);
// add a page
$pdf->AddPage();

$html = file_get_contents('fetch_form.php');

$pdf->writeHTML($html, true, 0, true, 0);

$js = file_get_contents('fetch_form.js');
/*
$js = <<<EOD
FetchUserData = function(regNo)
{
var params =
{
cVerb: "GET",
cURL: "http://localhost/PdfForms/api_user.php?regno=" + regNo,
oHandler:
{
response: function(msg, uri, e)
{
if(e != undefined && msg.error != undefined) {
app.alert("Failed to : "+ msg.error);
} else {
app.alert("Fetched: " + msg);
}
}
}
};
Net.HTTP.request(params);
}
var prefilBtn = this.getField("fetch_regno");
var regno = this.getField("regno");
prefilBtn.setAction("MouseUp", "FetchUserData("+regno.value+")");

var name = this.getField("name");
var email = this.getField("email");
var phone = this.getField("phone");
var address = this.getField("address");
EOD;
 */
$pdf->IncludeJS($js);

// reset pointer to the last page
$pdf->lastPage();

//Close and output PDF document
$pdf->Output('sample_form.pdf', 'D');
